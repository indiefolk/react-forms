import React, { Component } from 'react';
import getOr from 'lodash/fp/getOr';
import { action } from '@storybook/addon-actions';

export default (stateName, stateUpdaterName, initialState) => func => {
  const stateUpdateAction = action(stateUpdaterName);

  class State extends Component {
    state = {
      value: initialState,
    };

    constructor(props) {
      super(props);
      Object.defineProperty(this.setValue, 'name', { value: stateUpdaterName });
    }

    setValue = data => {
      stateUpdateAction(data);
      this.setState({ value: getOr(data, 'target.value', data) });
    };

    render() {
      return this.props.children({
        [stateName]: this.state.value,
        [stateUpdaterName]: this.setValue,
      });
    }
  }

  return () => <State>{func}</State>;
};
