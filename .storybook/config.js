import { configure } from '@storybook/react';
import { setOptions } from '@storybook/addon-options';
import { setDefaults } from '@storybook/addon-info';
import u from 'updeep';

import './storybook.css';
import 'bootstrap/dist/css/bootstrap.min.css';

setOptions({
  name: 'react-form',
  url: '/',
  sidebarAnimations: false,
  selectedAddonPanel: 'README',
});

setDefaults({
  header: false,
  inline: true,
  propTables: false,
  styles: styles =>
    u(
      {
        infoBody: {
          border: 'none',
          boxShadow: 'none',
          padding: '0',
          margin: '20px 0 0',
        },
        infoStory: {
          margin: '0',
          border: '1px dashed #e5e5e5',
          padding: '20px',
        },
        source: {
          h1: {
            display: 'none',
          },
        },
      },
      styles
    ),
});

const reqs = [require.context('../src', true, /\.stories\.js$/)];

const loadStories = () => {
  reqs.forEach(ctx =>
    ctx
      .keys()
      .sort((a, b) => {
        if (a > b) return 1;
        return 0;
      })
      .forEach(filename => ctx(filename))
  );
};

configure(loadStories, module);
