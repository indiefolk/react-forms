import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import pick from 'lodash/fp/pick';

import { withContext } from '../../';
import { Consumer } from '../Field/context';

import './text-input.css';

const onChangeHandler = fn => e => {
  fn(e.target.value);
};

const TextInput = React.forwardRef(
  (
    {
      component: Component = 'input',
      invalid = false,
      name,
      onChange,
      value = '',
      required = true,
      ...rest
    },
    ref
  ) => (
    <Component
      aria-invalid={invalid}
      className={cn('TextInput', 'form-control', {
        'TextInput--invalid': invalid,
        'is-invalid': invalid,
      })}
      id={`${name}-field`}
      name={name}
      onChange={onChangeHandler(onChange)}
      ref={ref}
      type="text"
      required={required}
      value={value}
      {...rest}
    />
  )
);

TextInput.displayName = 'TextInput';

TextInput.propTypes = {
  component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  invalid: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  value: PropTypes.string,
  required: PropTypes.bool,
};

export { TextInput };

export default withContext(
  Consumer,
  pick(['name', 'value', 'onChange', 'onBlur', 'invalid', 'required'])
)(TextInput);
