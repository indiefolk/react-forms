import React from 'react';
import flow from 'lodash/fp/flow';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withReadme } from 'storybook-readme';
import { withInfo } from '@storybook/addon-info';
import withState from '../../../.storybook/utils/withState';

import README from './README.md';

import { TextInput } from './';

storiesOf('TextInput', module)
  .addDecorator(withKnobs)
  .addDecorator(withReadme(README))
  .add(
    'Default',
    flow(
      withInfo(),
      withState('value', 'handleChange', '')
    )(({ value, handleChange }) => (
      <TextInput onChange={handleChange} name="form-TextInput" value={value} />
    ))
  );
