import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import pick from 'lodash/fp/pick';

import { withContext } from '../../';
import { Consumer } from '../Field/context';

import './select.css';

const onChangeHandler = fn => e => {
  fn(e.target.value);
};

const renderPlaceholder = (value, placeholder) => {
  if (!value) {
    return <option value="">{placeholder}</option>;
  }
};

const Select = React.forwardRef(
  (
    {
      invalid = false,
      name,
      onChange,
      options,
      placeholder = 'Please select',
      required = true,
      value = '',
      ...rest
    },
    ref
  ) => (
    <select
      aria-invalid={invalid}
      className={cn('Select', 'form-control', {
        'Select--invalid': invalid,
        'is-invalid': invalid,
      })}
      id={`${name}-field`}
      name={name}
      onChange={onChangeHandler(onChange)}
      placeholder={placeholder}
      ref={ref}
      required={required}
      type="text"
      value={value}
      {...rest}
    >
      {renderPlaceholder(value, placeholder)}
      {options.map((option, i) => (
        <option key={i} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  )
);

Select.displayName = 'Select';

Select.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({ label: PropTypes.string, value: PropTypes.string })
  ),
  invalid: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
};

export { Select };

export default withContext(
  Consumer,
  pick(['name', 'value', 'onChange', 'onBlur', 'invalid', 'required'])
)(Select);
