import React from 'react';
import flow from 'lodash/fp/flow';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withReadme } from 'storybook-readme';
import { withInfo } from '@storybook/addon-info';
import withState from '../../../.storybook/utils/withState';

import README from './README.md';

import { Select } from './';

storiesOf('Select', module)
  .addDecorator(withKnobs)
  .addDecorator(withReadme(README))
  .add(
    'Default',
    flow(
      withInfo(),
      withState('value', 'handleChange', '')
    )(({ value, handleChange }) => (
      <Select
        onChange={handleChange}
        name="form-select"
        options={[
          { label: 'Option 1', value: '1' },
          { label: 'Option 2', value: '2' },
        ]}
        placeholder={'Please select'}
        value={value}
      />
    ))
  );
