import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withReadme } from 'storybook-readme';
import { withInfo } from '@storybook/addon-info';

import README from './README.md';

import { Label } from './';

storiesOf('Label', module)
  .addDecorator(withKnobs)
  .addDecorator(withReadme(README))
  .add(
    'Default',
    withInfo()(() => <Label name="form-label">This is a Label</Label>)
  );
