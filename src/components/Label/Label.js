import React from 'react';
import PropTypes from 'prop-types';

import { withContext } from '../../';
import { Consumer } from '../Field/context';

import styles from './label.css';

const Label = ({ children, name, ...rest }) => {
  return (
    <label
      className={styles['label']}
      htmlFor={name ? `${name}-field` : null}
      {...rest}
    >
      {children}
    </label>
  );
};

Label.propTypes = {
  children: PropTypes.node,
  name: PropTypes.string.isRequired,
};

export { Label };

export default withContext(Consumer, value => ({
  name: value.name,
}))(Label);
