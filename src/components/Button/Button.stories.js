import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withReadme } from 'storybook-readme';
import { withInfo } from '@storybook/addon-info';

import README from './README.md';

import { Button } from './';

storiesOf('Button', module)
  .addDecorator(withKnobs)
  .addDecorator(withReadme(README))
  .add('Default', withInfo()(() => <Button name="form-button">Submit</Button>));
