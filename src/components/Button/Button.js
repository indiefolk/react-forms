import React from 'react';
import cn from 'classnames';

import './button.css';

const Button = ({ htmlType = 'submit', children, ...rest }) => (
  <button
    className={cn('Button', 'btn', 'btn-primary')}
    type={htmlType}
    {...rest}
  >
    {children}
  </button>
);

export { Button };

export default Button;
