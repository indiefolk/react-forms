import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withReadme } from 'storybook-readme';
import { withInfo } from '@storybook/addon-info';
import { action } from '@storybook/addon-actions';
import * as yup from 'yup';

import README from './README.md';

import Form from './';
import Field from '../Field';
import FieldFeedback from '../FieldFeedback';
import TextInput from '../TextInput';
import Label from '../Label';
import Select from '../Select';
import Button from '../Button';

const initialValues = {
  title: '',
  firstName: '',
  middleName: '',
  surname: '',
  email: '',
};

const schema = yup.object().shape({
  title: yup.string().required('Enter your title'),
  firstName: yup.string().required('Enter your first name'),
  middleName: yup.string().required('Enter your middlename'),
  surname: yup.string().required('Enter your surname'),
  email: yup.string().required('Enter your email'),
});

const alertValues = values => {
  alert(JSON.stringify(values, null, 4));
};

storiesOf('Form', module)
  .addDecorator(withKnobs)
  .addDecorator(withReadme(README))
  .add('Demo', () => (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <h1 className="display-4">React Forms</h1>
        </div>
        <div className="col-12">
          <p className="lead">Fun with forms and React.context</p>
        </div>
      </div>
      <div className="row">
        <div className="col-12 col-sm-7 col-lg-6">
          <Form
            initialValues={initialValues}
            onSubmit={alertValues}
            validationSchema={schema}
          >
            <Field name="title">
              <Label>Title</Label>
              <Select
                options={[
                  { label: 'Mr', value: 'mr' },
                  { label: 'Mrs', value: 'mrs' },
                ]}
                placeholder="Please select"
              />
              <FieldFeedback />
            </Field>
            <Field name="firstName">
              <Label>First name</Label>
              <TextInput />
              <FieldFeedback />
            </Field>
            <Field name="surname">
              <Label>Surname</Label>
              <TextInput />
              <FieldFeedback />
            </Field>
            <Field name="email">
              <Label>Email</Label>
              <TextInput />
              <FieldFeedback />
            </Field>
            <Button>Submit</Button>
          </Form>
        </div>
      </div>
    </div>
  ));
