import React from 'react';
import PropTypes from 'prop-types';
import pick from 'lodash/fp/pick';
import { Formik } from 'formik';

const Form = ({
  children,
  enableReinitialize,
  initialValues,
  isInitialValid,
  onReset,
  onSubmit = () => {},
  validate,
  validateOnBlur,
  validateOnChange,
  validationSchema,
  ...props
}) => (
  <Formik
    enableReinitialize={enableReinitialize}
    initialValues={initialValues}
    isInitialValid={isInitialValid}
    onReset={onReset}
    onSubmit={onSubmit}
    validate={validate}
    validateOnBlur={validateOnBlur}
    validateOnChange={validateOnChange}
    validationSchema={validationSchema}
  >
    {formikProps => (
      <form
        className={'Form'}
        onSubmit={formikProps.handleSubmit}
        noValidate
        {...props}
      >
        {typeof children === 'function' ? children(formikProps) : children}
      </form>
    )}
  </Formik>
);

Form.displayName = 'Form';

Form.propTypes = {
  ...pick(
    [
      'enableReinitialize',
      'isInitialValid',
      'initialValues',
      'onReset',
      'onSubmit',
      'validate',
      'validateOnBlur',
      'validateOnChange',
      'validationSchema',
    ],
    Formik.propTypes
  ),
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
};

export { Form };

export default Form;
