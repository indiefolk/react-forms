import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withContext } from '../../';
import { Consumer } from '../Field/context';

const FieldFeedback = ({ name, children, ...rest }) => (
  <div
    className={cn('FieldFeedback', 'text-danger')}
    id={`${name}-feedback`}
    {...rest}
  >
    <span className={'FieldFeedback-message'}>{children}</span>
  </div>
);

FieldFeedback.displayName = 'FieldFeedback';

FieldFeedback.propTypes = {
  name: PropTypes.string,
  children: PropTypes.node,
};

export { FieldFeedback };

export default withContext(Consumer, value => ({
  name: value.name,
  children: value.invalid && value.error,
}))(FieldFeedback);
