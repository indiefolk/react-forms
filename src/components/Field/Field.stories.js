import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withReadme } from 'storybook-readme';
import { withInfo } from '@storybook/addon-info';

import { action } from '@storybook/addon-actions';

import README from './README.md';

import Field from './';
import Label from '../Label';

storiesOf('Field', module)
  .addDecorator(withKnobs)
  .addDecorator(withReadme(README))
  .add(
    'Default',
    withInfo()(() => (
      <Field name="test-field" onChange={action('handleChange')} value="">
        <p>A paragraph</p>
      </Field>
    ))
  )
  .add(
    'TextInput',
    withInfo()(() => (
      <Field name="test-field" onChange={action('handleChange')} value="">
        <Label>This is a Label</Label>
      </Field>
    ))
  );
