import React from 'react';
import PropTypes from 'prop-types';
import identity from 'lodash/fp/identity';
import isEmpty from 'lodash/fp/isEmpty';
import cn from 'classnames';
import { FastField as FormikField, connect, getIn } from 'formik';

import { Provider } from './context';
import './field.css';

const InnerField = ({
  children,
  component: Component = 'div',
  error,
  invalid,
  name,
  onBlur,
  onChange,
  required,
  touched,
  value,
  ...rest
}) => (
  <Component className={cn('Field', 'form-group')} {...rest}>
    <Provider
      value={{
        error,
        invalid,
        name,
        onBlur,
        onChange,
        required,
        touched,
        value,
      }}
    >
      {children}
    </Provider>
  </Component>
);

InnerField.propTypes = {
  children: PropTypes.node,
  component: PropTypes.oneOf(['div', 'fieldset']),
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  invalid: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  touched: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  value: PropTypes.any,
};

// formik prop supplied from 'formik' package by connect() higher order component
const Field = connect(
  ({ name, formik, onChange = () => {}, required, ...rest }) =>
    isEmpty(formik) || !name ? (
      <InnerField
        name={name}
        onChange={onChange}
        required={required}
        {...rest}
      />
    ) : (
      <FormikField name={name}>
        {({ field, form }) => {
          let error = getIn(form.errors, name);
          let touched = getIn(form.touched, name);

          if (typeof error === 'object') {
            error = Object.values(error)[0];
          }

          if (typeof touched === 'object') {
            touched = Object.values(touched).every(identity);
          }

          return (
            <InnerField
              name={name}
              value={field.value}
              onChange={value => {
                form.setFieldValue(name, value);
              }}
              onBlur={() => {
                form.setFieldTouched(name, true);
              }}
              error={error}
              touched={touched}
              invalid={!!(getIn(form.errors, name) && form.submitCount > 0)}
              required={required}
              {...rest}
            />
          );
        }}
      </FormikField>
    )
);

Field.displayName = 'Field';

Field.propTypes = InnerField.propTypes;

export { Field };

export default Field;
