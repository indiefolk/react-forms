import React, { forwardRef } from 'react';
import assignWith from 'lodash/fp/assignWith';
import isUndefined from 'lodash/fp/isUndefined';
// import hoistStatics from 'hoist-non-react-statics

const assignExceptUndefined = assignWith(
  (objValue, srcValue) => (isUndefined(srcValue) ? objValue : srcValue)
);

/* 
 A higher-order component to connect dumb components to a context <Consumer>.
 For example the Field has a context and passes data to its <Provider>, 
 then this is used to hook up things like Label, TextInput etc. With the matching <Consumer>.
 See https://reactjs.org/docs/context.html.
 The `mapContextToProps` function may be used to pick fields from the data to merge with the wrapped component's props.
 */

export default (Consumer, mapContextToProps = value => value) => Component => {
  // Forward refs
  // See https://reactjs.org/docs/forwarding-refs.html#forwarding-refs-in-higher-order-components
  const C = forwardRef((props, ref) => (
    <Consumer>
      {value => (
        <Component
          {...assignExceptUndefined(props, mapContextToProps(value, props))}
          ref={ref}
        />
      )}
    </Consumer>
  ));

  // Define a display name for the React devtools
  // See https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
  C.displayName = Component.displayName || Component.name || 'Component';
  C.WrappedComponent = Component;

  // Hoist statics
  // See https://reactjs.org/docs/higher-order-components.html#static-methods-must-be-copied-over
  // Waiting on issue: https://github.com/mridgway/hoist-non-react-statics/issues/48
  // return hoistStatics(C, Component)
  return C;
};
