# A Text input

```js
const TextInput = ({
  value
}) => (
  <input type="text" value={value} />
)


<TextInput value={value} />
```

# A Text input and a Field

```js
const Field = ({ value }) => (
  <div className="field">
    <TextInput value={value} />
  </div>
);

const TextInput = ({ value }) => <input type="text" value={value} />;
```

# Context hooks them together

```js
const {Provider, Consumer} = React.createContext(defaultValue);

const Field = ({
  children,
  value,
}) => (
  <Provider value={value}>
    <div className="field">
      {children}
    </div>
  </Provider>
)


const TextInput = ({
  props
}) => (
  <Consumer>
  { value => (
    <input type="text" value={value} {...props} />
  )}
  </Consumer>

);

...

<Field value={1}>
  <TextInput />
</Form>
```
